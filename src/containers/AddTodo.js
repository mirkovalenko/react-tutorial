import React from 'react'
import { connect } from 'react-redux'
import { addTodo } from '../components/todo-redux/actions'

let AddTodo = ({dispatch}) => {
    let input

    return (
        <div>
            <form
                onSubmit={e => {
                    e.preventDefault()

                    if (!input.value.trim()) {
                        return
                    }

                    dispatch(addTodo(input.value))
                }}
            >
                <input 
                    ref={node => {
                        input = node
                    }}
                />
                <button>
                    Add Todo
                </button>
            </form>
        </div>
    )
}
AddTodo = connect()(AddTodo)

export default AddTodo