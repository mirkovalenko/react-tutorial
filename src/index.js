import React from 'react';
import ReactDOM from 'react-dom';
import './style/main.css';
import Game from './components/tic-tac-toe/Game.jsx';

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);
