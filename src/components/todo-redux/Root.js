import { Router, Route } from 'react-router'
import { Provider } from 'react-redux'
import App from './App'

const Root = ({}) => (
    <Provider>
        <Router>
            <Route path='/' component={App} />
        </Router>
    </Provider>
)