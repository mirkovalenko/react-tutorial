const path = require('path');
const BUILD_DIR = path.resolve(__dirname, 'public');

module.exports = {
    entry: './src/index-redux.js',

    output: {
        filename: 'bundle.js',
        path: BUILD_DIR,
    },

    devServer: {
        contentBase: BUILD_DIR,
        historyApiFallBack: true
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: '/node_modules/',
                use: [
                    'react-hot-loader',
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['es2015', 'react']
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',                    
                    {
                        loader: 'css-loader',
                        options: {
                            minify: false
                        }
                    },
                ]
            }

        ]
    },

    devtool: 'inline-source-map',

    resolve: {
        extensions: ['.js', '.jsx', '.json', '*']
    }
}